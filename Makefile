all: compile

compile:
	mkdir -p build
	pdflatex -output-directory=build tda351.tex 
	pdflatex -output-directory=build tda351.tex

clean:
	rm -rf build
