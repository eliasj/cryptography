--
-- Writen by eliasj@student.chalmers.se
--
import Data.Text as T

trans :: [Int] -> String -> String
trans k s = Prelude.foldl1 (++) $ Prelude.map ( trans' k) $ T.chunksOf (Prelude.length k)  $ T.toLower $ T.filter (/=' ') $ T.pack s
	where
		trans' :: [Int] -> Text -> [Char]
		trans' [] _ = []
		trans' (k:ks) s =  (T.index s k): trans' ks s


abc =  "abcdefghijklmnopqrstuvwxyz"
subs :: Int -> String -> Text
subs k s = subs' k s (abc ++ abc)
desubs :: Int -> String -> Text
desubs k s = subs' k s (Prelude.reverse  (abc ++ abc))
subs' k s a = T.map (subs'' k (T.pack a) )  $ T.toLower $ T.filter (/=' ') $ T.pack s
	where
		subs'' :: Int -> Text -> Char -> Char
		subs'' k s a = T.index (T.dropWhile (/=a) s) k

vigenere :: String -> String -> String
vigenere s k = map2 test (T.unpack $ T.toLower $ T.filter (/=' ') $ T.pack s) (cycle k)
test :: Char -> Char -> Char
test a b | ((fromEnum a) + ((fromEnum b) - 97)) <= 122 = toEnum((fromEnum a) + ((fromEnum b) - 97))
		 | otherwise  =  toEnum((fromEnum a) + ((fromEnum b) - 97) - 26)


-- Help functions
map2 :: ( a -> b -> c) -> [a] -> [b] -> [c]
map2 _ [] _ = []
map2 _ _ [] = [] 
map2 f (a:as) (b:bs) = (f a b) : map2 f as bs
