

xor True False = True
xor False True = True
xor _ _ = False

byte :: [Bool] -> [[Bool]]
byte b = byte' b [[]] 8

byte' :: [Bool] -> [[Bool]] -> Int -> [[Bool]]
byte' [] as _ = as
byte' b as 0 = byte' b ([]:as) 8
byte' (b:bs) (a:as) i = byte' bs ((b:a):as) (i-1)

map2 :: (a -> b -> c) -> [a] -> [b] -> [c]
map2 _ [] _ = []
map2 _ _ [] = []
map2 f (a:as) (b:bs) = (f a b):(map2 f as bs)
